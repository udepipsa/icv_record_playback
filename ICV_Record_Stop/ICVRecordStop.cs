﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Winium;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace ICV_Record_Stop
{
    [TestClass]
    public class ICVRecordStop
    {
        Random rnd = new Random();
        DesktopOptions dop = new DesktopOptions();

        public WiniumDriver driver;


        [TestMethod]
        [Ignore]
        public void TestMethod2()
        {
            dop.ApplicationPath = @"C:\Program Files (x86)\Panasonic\UEMS2\Uploader\uploaderClientApplication.exe";
            driver = new WiniumDriver(@"C:\Users\Manesh.Nallavalli\Downloads\Winium.Desktop.Driver", dop);
            driver.Keyboard.SendKeys(".\\Administrator");
            Thread.Sleep(5000);
        }

        [TestMethod]
        [Ignore]
        public void email(string sFromAddress, string sToAddress)
        {

            MailMessage msg;
            String userName = "no-reply@qa.ude.com";
            String password = "no-reply";
            //msg = new MailMessage("no-reply@qa.ude.com", "manesh@qa.ude.com;manesh@qa.ude.com");
            msg = new MailMessage(sFromAddress, sToAddress);
            msg.Subject = "Test";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name: MAnesh");
            sb.AppendLine("Mobile Number: 123456789");
            sb.AppendLine("Email: xxxxx");
            sb.AppendLine("Drop Downlist Name: test");
            msg.Body = sb.ToString();
            Attachment attach = new Attachment(@"\\UDE-Terastation\PIPSADevShare\02-QATeam\Reports\2021-01-07 13-45-30\index.html");
            msg.Attachments.Add(attach);
            SmtpClient SmtpClient = new SmtpClient();
            SmtpClient.Host = "10.10.1.17";
            SmtpClient.Port = 25;
            SmtpClient.EnableSsl = false;
            SmtpClient.UseDefaultCredentials = false;
            SmtpClient.Credentials = new System.Net.NetworkCredential(userName, password);
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
           // SmtpClient.Send(msg);            

        }



        [TestMethod]
        public void TestMethod1()
        {

            DateTime runtime = DateTime.Now.AddDays(7);
            //DateTime runtime = DateTime.Now.AddMinutes(3);
            while (DateTime.Now < runtime)
            {
                try
                {


                    dop.ApplicationPath = @"C:\Program Files (x86)\Panasonic\ICV\FE\ICV_FE.exe";
                    driver = new WiniumDriver(@"C:\Users\Manesh.Nallavalli\Downloads\Winium.Desktop.Driver", dop);
                    Thread.Sleep(30000);
                    driver.FindElementById("officer1PasswordGroupBox").SendKeys("Nov2020");
                    driver.FindElementById("loginButton").Click();
                    Thread.Sleep(10000);
                    //int i = 1;
                    while (DateTime.Now < runtime)
                    // while (i < 2)
                    {
                        try
                        {
                            driver.FindElementById("recordButton").Click();
                            Thread.Sleep(TimeSpan.FromMinutes(rnd.Next(5, 15)));
                            driver.FindElementById("stopButton").Click();
                            Thread.Sleep(5000);
                            driver.FindElementById("okButton").Click();
                            Thread.Sleep(TimeSpan.FromMinutes(2));
                        }
                        catch (Exception)
                        {
                            //if (driver.FindElementById("Button1").Displayed)
                            //    driver.FindElementById("Button1").Click();``````
                            DesktopScreenCapture.ImageCapture(driver);
                            driver.FindElementById("CloseBtn").Click();
                            driver.FindElementById("exitButton").Click();
                        }
                        // i++;
                    }
                }
                catch (Exception)
                {

                    DesktopScreenCapture.ImageCapture(driver);
                    driver.FindElementById("CloseBtn").Click();
                    driver.FindElementById("exitButton").Click();

                } 
            }
        }

        [TestMethod]

        public void GetFiles()
        {
            var targetPath = @"\\udeqa2.ude.local\UDEStorage\Default";
            var oldfiles = Directory.GetFiles(targetPath, "*.av3", SearchOption.AllDirectories).Union(Directory.GetFiles(targetPath, "*.mp4", SearchOption.AllDirectories));
            Console.WriteLine(oldfiles.Count());
        }

    }
}
