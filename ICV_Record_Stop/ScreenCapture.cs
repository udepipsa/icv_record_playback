﻿using OpenQA.Selenium;
using OpenQA.Selenium.Winium;
using System;
using System.Drawing.Imaging;

namespace ICV_Record_Stop
{
    public static class DesktopScreenCapture 
	{
        public static void ImageCapture(WiniumDriver driver)
        {
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            string Runname = "ICV-" + DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss");
            string screenshotfilename = "C:\\screenshots\\" + Runname + ".png";
            ss.SaveAsFile(screenshotfilename, ImageFormat.Png);
            //ss.SaveAsFile("ss.png", ImageFormat.Png);
        }
    }
}

